#!/bin/bash

NEWLOC=`curl -k "https://www.virtualbox.org/wiki/Downloads" 2>/dev/null | grep 'OSX.dmg' | awk -F "=" '{ print $3 }' | sed 's/\"//;s/\"\>\<span\ class//' | tr -d '\r'`

if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi
