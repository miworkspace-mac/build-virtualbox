#!/bin/bash -ex

# CONFIG
prefix="VirtualBox"
suffix=""
munki_package_name="VirtualBox"

url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36' "${url}"

mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

# copy original pkg
apppkg=`ls ${mountpoint}/*.pkg`
mkdir -p pkg-root/private/tmp
cp "${apppkg}" pkg-root/private/tmp/virtualbox.pkg

# expand pkg to key files directory
mkdir build-root
/usr/sbin/pkgutil --expand "${apppkg}" pkg
(cd build-root; pax -rz -f ../pkg/VirtualBox.pkg/Payload)
hdiutil detach "${mountpoint}"

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" build-root/*.app/Contents/Info.plist`

#build PKG
/usr/bin/pkgbuild --root pkg-root/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --version ${version} --scripts scripts app_final.pkg


## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app_final.pkg "${key_files}" | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root/\/Applications/' app.plist

# Build pkginfo
#/usr/local/munki/makepkginfo app.dmg > app.plist

plist=`pwd`/app.plist

# Change path and other details in the plist
# defaults write "${plist}" blocking_applications -array "VirtualBox.app"
# defaults write "${plist}" unattended_install -bool YES

defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.13.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" supported_architectures -array x86_64
defaults write "${plist}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
displayname=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${displayname}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"


# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app_final.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
